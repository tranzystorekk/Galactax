/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galactax;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 * A Pause Screen - a minute of respite from 
 * the horror and solitude sof endless space travel.
 * <p>
 * Contains selectable options.
 *
 * @author marcin
 * @see Screen
 */
public class PauseScreen implements Screen {
    
    final Galactax game;
    
    Options option;
    
    /**
     * Options in the Pause Menu.
     * <p>
     * Has special methods for quicker navigation.
     */
    enum Options {
	
	RESUME {
	    @Override
	    public Options prev() {
		
		return Options.RETURN_TO_MENU;
	    }
	},
	
	SETTINGS,
	
	RETURN_TO_MENU {
	    @Override
	    public Options next() {
		
		return Options.RESUME;
	    }
	};
	
	public Options next() {
	    
	    return values()[ordinal() + 1];
	}
	
	public Options prev() {
	    
	    return values()[ordinal() - 1];
	}
    }
    
    /**
     * Constructs a PauseScreen with a corresponding Galactax main class.
     * 
     * @param g reference to the main game class
     */
    public PauseScreen(final Galactax g) {
        
	this.game = g;
    }

    @Override
    public void show() {
        
	option = Options.RESUME;
    }

    @Override
    public void render(float delta) {
        
	game.view.render(delta);
	
	game.view.shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
	game.view.shapeRenderer.setColor(Constants.MENU_BACKGROUND_COLOR);
	game.view.shapeRenderer.rect(0f, 0f, Constants.GAME_WIDTH, Constants.GAME_HEIGHT);
	game.view.shapeRenderer.end();
	
	game.view.batch.begin();
	
	game.view.font.draw(game.view.batch, "Resume", 150, 500);
	game.view.font.draw(game.view.batch, "Settings", 150, 440);
	game.view.font.draw(game.view.batch, "Return to Main Menu", 150, 380);
	
	game.view.batch.draw(game.view.selectorTexture, 150 - 20 - game.view.selectorTexture.getWidth(),
							500 - 60 * option.ordinal() - game.view.selectorTexture.getHeight());
	
	game.view.batch.end();
	
	if(Gdx.input.isKeyJustPressed(Constants.PAUSE_KEY)) {
	    
	    game.launchGame();
	}
	
	if(Gdx.input.isKeyJustPressed(Constants.SELECT_KEY)) {
	    
	    switch(option) {
		
		case RESUME:
		    game.launchGame();
		    break;
		
		case SETTINGS:
		    game.launchSettings();
		    break;
		
		case RETURN_TO_MENU:
		    game.launchMenu();
		    break;
	    }
	}
	
	if(Gdx.input.isKeyJustPressed(Constants.UP_KEY)) {
	    
	    option = option.prev();
	}
	
	else if(Gdx.input.isKeyJustPressed(Constants.DOWN_KEY)) {
	    
	    option = option.next();
	}
    }

    @Override
    public void resize(int width, int height) {
        
	game.view.resize(width, height);
    }

    @Override
    public void pause() {
        
	
    }

    @Override
    public void resume() {
        
	
    }

    @Override
    public void hide() {
        
	
    }

    @Override
    public void dispose() {
        
	
    }
    
}
