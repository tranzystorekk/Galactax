package com.galactax;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;

/**
 * Main class that manages screens based on prompts received from them.
 * Derives from GDX Game class which has methods for coexisting with screens.
 * 
 * @see Game
 * @author marcin
 */
public class Galactax extends Game {
	
        Screen pauseScreen;
        Screen menuScreen;
	Screen settingsScreen;
	
	Screen gameOverScreen;
	Screen gameScreen;
	
	boolean gameLaunched;
	boolean gameOvered;
	boolean paused;
	
	private int lastScore;
    
	public View view;
	
	/**
	 * Construct a new main class
	 */
	public Galactax() {
	    
	    gameLaunched = false;
	    gameOvered = false;
	    paused = false;
	}
	
	public void launchMenu() {
	    
	    if(gameLaunched) {
		
		gameScreen.dispose();
		gameLaunched = false;
	    }
	    
	    else if(gameOvered) {
		
		gameOverScreen.dispose();
		gameOvered = false;
	    }
	    
	    paused = false;
	    
	    this.setScreen(menuScreen);
	}
	
	public void launchGame() {
	    
	    if(!gameLaunched) {
		
		gameLaunched = true;
		
		gameScreen = new GameScreen(this);
	    }
	    
	    paused = false;
	    
	    this.setScreen(gameScreen);
	}
	
	public void launchSettings() {
	    
	    this.setScreen(settingsScreen);
	}
	
	public void pauseGame() {
	    
	    paused = true;
	    
	    this.setScreen(pauseScreen);
	}
	
	public boolean isPaused() {
	    
	    return this.paused;
	}
	
	public void gameOver(int score) {
	    
	    gameLaunched = false;
	    gameScreen.dispose();
	    
	    gameOvered = true;
	    
	    gameOverScreen = new GameOverScreen(this, score);
	    
	    this.setScreen(gameOverScreen);
	}
	
	@Override
	public void create () {
	    
	    view = new View();
	    
	    pauseScreen = new PauseScreen(this);
	    settingsScreen = new SettingsScreen(this);
	    menuScreen = new MainMenuScreen(this);
	    
	    this.setScreen(menuScreen);
	}

	@Override
	public void render () {
		
	    //draw black sub-background (for bars)
	    Gdx.gl.glClearColor(0, 0, 0, 1);
	    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	    
	    super.render();
	}
	
	@Override
	public void dispose () {
		
            pauseScreen.dispose();
	    settingsScreen.dispose();
	    menuScreen.dispose();
	    
	    if(gameLaunched)
		gameScreen.dispose();
	    
	    else if(gameOvered)
		gameOverScreen.dispose();
	    
	    view.dispose();
	}
}
