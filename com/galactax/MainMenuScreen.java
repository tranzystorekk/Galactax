/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galactax;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 * A screen for the Main Menu, with selectable options.
 * 
 * @see Screen
 * @author marcin
 */
public class MainMenuScreen implements Screen {
    
    final Galactax game;
    
    Texture gameLogoTexture;
    
    Options option;
    
    enum Options {
	
	PLAY {
	    @Override
	    public Options prev() {
		
		return Options.EXIT;
	    }
	},
	
	SETTINGS,
	//HIGH_SCORES,
	
	EXIT {
	    @Override
	    public Options next() {
		
		return Options.PLAY;
	    }
	};
	
	public Options next() {
	    
	    return values()[ordinal() + 1];
	}
	
	public Options prev() {
	    
	    return values()[ordinal() - 1];
	}
    }
    
    public MainMenuScreen(final Galactax g) {
        
        this.game = g;
	
	gameLogoTexture = new Texture("game_logo.png");
	
	//option = Options.PLAY;
    }

    @Override
    public void show() {
        
        option = Options.PLAY;
    }

    @Override
    public void render(float delta) {
        
        game.view.render(delta);
	
	game.view.shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
	game.view.shapeRenderer.setColor(Constants.MENU_BACKGROUND_COLOR);
	game.view.shapeRenderer.rect(0, 0, Constants.GAME_WIDTH, Constants.GAME_HEIGHT);
	game.view.shapeRenderer.end();
	
	game.view.batch.begin();
	
	game.view.font.draw(game.view.batch, "New Game", 150, 500);
	game.view.font.draw(game.view.batch, "Settings", 150, 440);
	//game.view.font.draw(game.view.batch, "High Scores", 150, 380);
	game.view.font.draw(game.view.batch, "Exit", 150, 380);
	
	game.view.batch.draw(gameLogoTexture, 70, 800 - 128 - 10);
	game.view.batch.draw(game.view.selectorTexture, 150 - 20 - game.view.selectorTexture.getWidth(),
							500 - 60 * option.ordinal() - game.view.selectorTexture.getHeight());
	
	game.view.batch.end();
	
	if(Gdx.input.isKeyJustPressed(Constants.SELECT_KEY)) {
	    
	    switch(option) {
		
		case PLAY:
		    game.launchGame();
		    break;
		
		case SETTINGS:
		    game.launchSettings();
		    break;
		
		case EXIT:
		    Gdx.app.exit();
		    break;
	    }
	}
	
	if(Gdx.input.isKeyJustPressed(Constants.UP_KEY)) {
	    
	    option = option.prev();
	}
	
	else if(Gdx.input.isKeyJustPressed(Constants.DOWN_KEY)) {
	    
	    option = option.next();
	}
    }

    @Override
    public void resize(int width, int height) {
        
        game.view.resize(width, height);
    }

    @Override
    public void pause() {
        
        
    }

    @Override
    public void resume() {
        
        
    }

    @Override
    public void hide() {
        
        
    }

    @Override
    public void dispose() {
	
	gameLogoTexture.dispose();
    }
    
}
