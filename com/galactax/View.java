/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galactax;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.viewport.*;

/**
 * View class holding essential rendering objects from the libGDX library.
 *
 * @author marcin
 */
public class View {
    
    private Viewport viewport;
    private OrthographicCamera camera;
    
    public SpriteBatch batch;
    
    public Texture selectorTexture;
    
    public BitmapFont font;
    
    public ShapeRenderer shapeRenderer;
    
    private int windowedWidth;
    private int windowedHeight;
    
    /**
     * Constructs the view object, initializing libGDX components.
     */
    public View() {
	
	camera = new OrthographicCamera();
	camera.setToOrtho(false, Constants.GAME_WIDTH, Constants.GAME_HEIGHT);
	
	viewport = new FitViewport(Constants.GAME_WIDTH, Constants.GAME_HEIGHT, camera);
	
	batch = new SpriteBatch();
	
	selectorTexture = new Texture("selector_big.png");
	
	font = new BitmapFont(Gdx.files.internal("neoletters.fnt"));
	
	shapeRenderer = new ShapeRenderer();
	
	windowedWidth = Constants.GAME_WIDTH;
	windowedHeight = Constants.GAME_HEIGHT;
    }
    
    /**
     * 
     * @return whether or not the game is currently in full screen mode
     */
    public boolean isFullscreenOn() {
	
	return Gdx.graphics.isFullscreen();
    }
    
    /**
     * Toggles full screen mode.
     */
    public void toggleFullscreen() {
	
	if(Gdx.graphics.isFullscreen()) {
	    
	    Gdx.graphics.setWindowedMode(windowedWidth, windowedHeight);
	}
	
	else
	    Gdx.graphics.setFullscreenMode(Gdx.graphics.getDisplayMode());
    }
    
    /**
     * Resets resolution to the game's initial one.
     */
    public void resetResolution() {
	
	windowedWidth = Constants.GAME_WIDTH;
	windowedHeight = Constants.GAME_HEIGHT;
	
	Gdx.graphics.setWindowedMode(windowedWidth, windowedHeight);
    }
    
    /**
     * Performs essential rendering-related operations (camera, viewport etc.)
     * 
     * @param delta passed portion of time in seconds (unused; originates in libGDX interface)
     */
    public void render(float delta) {
	
	camera.update();
	
	viewport.apply();
	
	batch.setProjectionMatrix(camera.combined);
    }
    
    /**
     * Performs window resize with necessary scaling.
     * 
     * @param w width resize parameter
     * @param h height resize parameter
     */
    public void resize(int w, int h) {
	
	if(!Gdx.graphics.isFullscreen()) {
	    windowedWidth = w;
	    windowedHeight = h;
	}
	
	viewport.update(w, h);
    }
    
    /**
     * Perform garbage collection necessary for libGDX components.
     */
    public void dispose() {
	
	batch.dispose();
	
	selectorTexture.dispose();
	font.dispose();
	
	shapeRenderer.dispose();
    }
}
