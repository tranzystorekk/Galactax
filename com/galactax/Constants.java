/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galactax;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;

/**
 * A non-initializable class used for holding game-critical const values.
 *
 * @author marcin
 */
public class Constants {
    
    //prevent class from being instatiated
    private Constants() {}
    
    //game screen measures
    public static final int GAME_WIDTH = 600;
    public static final int GAME_HEIGHT = 800;
    
    //star speed range
    public static final float MIN_STAR_SPEED = 100f;
    public static final float MAX_STAR_SPEED = 1000f;
    
    //various speeds
    public static final float SPACESHIP_SPEED = 500f;
    public static final float PROJECTILE_SPEED = 1000f;
    public static final float ENEMY_SPEED = 300f;
    public static final float[] ASTEROID_SPEEDS = {400f, 300f, 200f};
    
    //variables for enemy shooting implementation
    public static final float ENEMY_SHOOT_TIME = 0.1f;
    public static final float ENEMY_WAIT_TIME = 0.6f;
    public static final int ENEMY_NUMBER_OF_SHOTS = 1;
    
    public static final float WAVE_TIME = 4f;
    public static final float WAIT_BETWEEN_WAVES_TIME = 6f;
    
    //point values
    public static final int ENEMY_POINTS = 300;
    public static final int[] ASTEROID_POINTS = {100, 250, 500};
    
    public static final float STAR_SPAWN_TIME = 0.3f;
    
    public static final float ENEMY_SIZE = 64f;
    
    public static final float ENEMY_SPAWN_TIME = ENEMY_SIZE / ENEMY_SPEED;
    
    public static final float ENEMY_MIN_HEIGHT = 400f;
    public static final float ENEMY_MAX_HEIGHT = 720f;
    
    public static final int PLAYER_HEALTH = 4;
    
    //bakcground colors
    public static final Color MENU_BACKGROUND_COLOR = new Color(0.1f, 0.1f, 0.1f, 1f);
    public static final Color GAME_BACKGROUND_COLOR = new Color(0.2f, 0.2f, 0.2f, 1f);
    
    //keys
    public static final int SHOOT_KEY = Input.Keys.X;
    public static final int UP_KEY = Input.Keys.UP;
    public static final int DOWN_KEY = Input.Keys.DOWN;
    public static final int LEFT_KEY = Input.Keys.LEFT;
    public static final int RIGHT_KEY = Input.Keys.RIGHT;
    public static final int PAUSE_KEY = Input.Keys.ESCAPE;
    public static final int SELECT_KEY = Input.Keys.ENTER;
    
    public static final int[] ASTEROID_SIZES = { 32, 64, 128 };
    
    public static final float[] ASTEROID_SPAWN_TIMES = { Constants.ASTEROID_SIZES[0] / Constants.ASTEROID_SPEEDS[0],
							 Constants.ASTEROID_SIZES[1] / Constants.ASTEROID_SPEEDS[1],
							 Constants.ASTEROID_SIZES[2] / Constants.ASTEROID_SPEEDS[2] };
    
    //asteroid polygon vertex arrays
    public static final float[] SMALL_VERTICES = { 0f, 8f, 0f, 24f, 8f, 32f, 24f, 32f,
						   32f, 24f, 32f, 8f, 24f, 0f, 8f, 0f };
    
    public static final float[] MEDIUM_VERTICES = { 0f, 32f, 16f, 64f, 64f, 56f,
						    64f, 24f, 40f, 0f, 16f, 8f };
    
    public static final float[] BIG_VERTICES = { 0f, 56f, 8f, 112f, 88f, 128f,
						 128f, 72f, 128f, 24f, 56f, 0f };
}
