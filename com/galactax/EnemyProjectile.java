/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galactax;

import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;

/**
 * This class represents a high energy plasma projectile emitted
 * by combat class cannons used by developed alien civilizations.
 * <p>
 * For some reason it travels significantly slower than light.
 *
 * @author marcin
 * @see GameObject
 */
public class EnemyProjectile extends GameObject {
    
    Polygon collisionPolygon;
    
    /**
     * Constructs a new EnemyProjectile using a pre-calculated trajectory vector.
     * 
     * @param x
     * @param y
     * @param w
     * @param h
     * @param rv pre-calculated trajectory vector
     * @see GameObject#GameObject(float, float, float, float) 
     */
    public EnemyProjectile(float x, float y, float w, float h, Vector2 rv) {
	
	super(x, y, w, h);
	
	speed = new Vector2(rv);
	
	collisionPolygon = new Polygon(new float[] { 0f, 0f, rect.width, 0f, rect.width, rect.height, 0f, rect.height });
	
	collisionPolygon.setPosition(rect.x, rect.y);
	
	collisionPolygon.setOrigin(rect.width / 2, rect.height / 2);
	collisionPolygon.setRotation(speed.angle());
	collisionPolygon.dirty();
    }
    
    @Override
    public void applySpeed(float delta) {
	
	super.applySpeed(delta);
	
	collisionPolygon.setPosition(collisionPolygon.getX() + speed.x * delta, collisionPolygon.getY() + speed.y * delta);
    }
    
    /**
     * Get angle of the speed vector.
     * 
     * @return angle of vector in degrees
     */
    public float getAngle() {
	
	return speed.angle();
    }
}
