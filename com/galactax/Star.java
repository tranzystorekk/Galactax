/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galactax;

import com.badlogic.gdx.math.Vector2;

/**
 * This class represents background stars
 * flying past for dynamic movement illusion.
 * <p>
 * Some pilots believe they are just lights stuck with glue
 * to a scrolling wall in the distance.
 *
 * @author marcin
 * @see GameObject
 */
public class Star extends GameObject {
    
    private boolean small;
    
    /**
     * Constructs a new star.
     * 
     * @param x
     * @param y
     * @param w
     * @param h
     * @param sp y-component of the star's speed vector
     * @param sm true = small, false = big
     * @see GameObject#GameObject(float, float, float, float)
     */
    public Star(float x, float y, float w, float h, float sp, boolean sm) {
	
	super(x, y, w, h);
	
	this.speed = new Vector2(0f, -sp);
	
	this.small = sm;
    }
    
    /**
     * 
     * @return whether the star is small
     */
    public boolean isSmall() {
	
	return small;
    }
    
    /**
     * 
     * @param delta
     * @see GameObject#applySpeed(float) 
     */
    @Override
    public void applySpeed(float delta) {
	
	rect.y += speed.y * delta;
    }
}
