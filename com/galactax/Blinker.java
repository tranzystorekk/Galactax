/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galactax;

/**
 * A special class for calculating blink intervals for the Spaceship
 * 
 * @see Spaceship
 * @author marcin
 */
public class Blinker {
    
    private float blinkingTime;
    private int blinkingFrames;
    
    private boolean isBlinking;
    private int blinkFrameCounter;
    private float blinkTimer;
    
    /**
     * Construct a new Blinker.
     * 
     * @param frames total blink frames (visible + invisible)
     * @param time blink time in seconds
     */
    public Blinker(int frames, float time) {
	
	this.blinkingFrames = frames;
	this.blinkingTime = time;
	
	this.isBlinking = false;
	this.blinkFrameCounter = 0;
	this.blinkTimer = 0f;
    }
    
    /**
     * Update blink timers and tell whether spaceship should be visible.
     * 
     * @param delta passed time portion in seconds
     * @return whether or not the spaceship should be visible in the current frame
     */
    public boolean shouldDisplay(float delta) {
	
	if(isBlinking) {
	    
	    blinkTimer += delta;
	    
	    if(++blinkFrameCounter % blinkingFrames != 0)
		    return false;
	    
	    if(blinkTimer > blinkingTime) {
		
		blinkTimer = 0f;
		blinkFrameCounter = 0;
		isBlinking = false;
	    }
	}
	
	return true;
    }
    
    /**
     * 
     * @return whether or not this object is currently in blinking mode
     */
    public boolean isBlinking() {
	
	return isBlinking;
    }
    
    /**
     * Switch into blinking mode, if not already in it.
     */
    public void startBlinking() {
	
	if(!isBlinking)
	    isBlinking = true;
    }
}
