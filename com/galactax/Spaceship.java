/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galactax;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * This class represents the player's space vehicle (not an egg!)
 * <p>
 * Contains a collision rectangle.
 *
 * @author marcin
 * @see GameObject
 */
public class Spaceship extends GameObject {
    
    private int health;
    
    private Blinker blinker;
    
    public Rectangle collisionRectangle;
    
    /**
     * 
     * @param x
     * @param y
     * @param w
     * @param h
     * @see GameObject#GameObject(float, float, float, float)
     */
    public Spaceship(float x, float y, float w, float h) {
	
	super(x, y, w, h);
	
	speed = new Vector2();
	
	health = Constants.PLAYER_HEALTH;
	
	blinker = new Blinker(3, 1.25f);
    }
    
    /**
     * Set speed vector's vertical component.
     * 
     * @param v value set to the speed vector
     */
    public void setVerticalSpeed(float v) {
	
	speed.set(speed.x, v);
    }
    
    /**
     * Set speed vector's horizontal component.
     * 
     * @param v value set to the speed vector
     */
    public void setHorizontalSpeed(float v) {
	
	speed.set(v, speed.y);
    }
    
    @Override
    public void applySpeed(float delta) {
	
	super.applySpeed(delta);
	
	collisionRectangle.x += speed.x * delta;
	collisionRectangle.y += speed.y * delta;
    }
    
    /**
     * Reset speed vector to (0, 0).
     */
    public void resetSpeed() {
	
	speed.setZero();
    }
    
    /**
     * React to receiving damage (health decrease, blinking).
     */
    public void dealDamage() {
	
	--health;
	
	blinker.startBlinking();
    }
    
    /**
     * Uses its blinker's properties to inform whether it should currently receive damage.
     * 
     * @return whether or not the Spaceship is currently vulnerable to damage
     */
    public boolean isVulnerable() {
	
	return !blinker.isBlinking();
    }
    
    /**
     * Returns blinker object for performing operations on it.
     * 
     * @return the Spaceship's blinker
     * @see Blinker
     */
    public Blinker getBlinker() {
	
	return this.blinker;
    }
    
    /**
     * 
     * @return the Spaceship's current health
     */
    public int getHealth() {
	
	return health;
    }
}
