/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galactax;

import com.badlogic.gdx.math.Vector2;

/**
 * This class represents an Enemy to the Player and all the human race.
 * <p>
 * Shoot on sight. Or it will shoot you.
 *
 * @author marcin
 * @see GameObject
 */
public class Enemy extends GameObject {
    
    private float timer;
    private int shotCounter;
    
    private boolean isShooting;
    
    /**
     * Create a new Enemy.
     * <p>
     * Contains a collision rectangle.
     * Also fiendishly calculates when it shall be shooting at the Player.
     * 
     * @param x
     * @param y
     * @param w
     * @param h
     * @param sp horizontal speed vector component
     * @see GameObject#GameObject(float, float, float, float) 
     */
    public Enemy(float x, float y, float w, float h, float sp) {
	super(x, y, w, h);
	
	speed = new Vector2(sp, 0f);
	
	timer = 0f;
	shotCounter = 0;
	
	isShooting = false;
    }
    
    /**
     * Updates the shooting timer and informs whether this Enemy
     * should be shooting in the current frame.
     * 
     * @param delta
     * @return whether or not this Enemy should be shooting
     */
    public boolean updateTimer(float delta) {
	
	timer += delta;
	
	if(isShooting) {
	    
	    if(timer > Constants.ENEMY_SHOOT_TIME) {
		
		if(++shotCounter >= Constants.ENEMY_NUMBER_OF_SHOTS) {
		    
		    shotCounter = 0;
		    
		    isShooting = false;
		}
		
		timer = 0f;
		
		return true;
	    }
	}
	
	else {
	    
	    if(timer > Constants.ENEMY_WAIT_TIME) {
		
		timer = 0f;
		
		isShooting = true;
	    }
	}
	
	return false;
    } 
}
